using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMove1 : MonoBehaviour
{
    //-1 0.31
    [SerializeField] private bool m_start = false;
    [SerializeField] private Rigidbody2D m_rigid;
    private Vector2 m_velodown= new Vector2(0,-3);
    private Vector2 m_velostop = new Vector2(0, 0);
    private Vector2 m_velodie = new Vector2(0, -20);
    private Vector2 m_velomax = new Vector2(0, 7.2f);
    private Vector3 m_rotate = new Vector3(0, 0, -1);
    private const float m_angle = 90; 
    [SerializeField] private AudioSource m_fly;
    
    private Vector2 m_translate = new Vector2(0, 1);
    [SerializeField] private BirdInput m_input;
    

    // Start is called before the first frame update

    private void Start()
    {
        m_input.OnClickMouse.AddListener(Jump);
    }

    private void OnDestroy()
    {
        m_input.OnClickMouse.RemoveListener(Jump);
    }
    public void Force()
    {
        transform.Translate(m_translate); 
    }
    public void Jump()
    {
        if (m_start)
        {

            if (m_rigid != null)
            {
                m_rigid.gravityScale = 3f;
                m_fly.Play();
                //transform.Translate(new Vector2(0,0.7f)); 
                m_rigid.AddForce(new Vector2(0, 800));
                
            }
        }
        else
        {
            m_rigid.gravityScale = 0;
        };
    }

    private void Update()
    {
        if (m_rigid.velocity.y > 7.2f)
        {
            m_rigid.velocity = m_velomax;
            Debug.Log(">2");
        }
    }
    public void Die()
    {
        m_start = false;
        transform.Rotate(m_rotate, m_angle); 
        m_rigid.velocity = m_velodie;
        
        
    }
    public void Rotateaagian()
    {
        transform.rotation = Quaternion.Euler(Vector3.zero);
    }
    public void StartMove()
    {

        m_rigid.velocity = m_velodown;
        m_start = true;
    }
    public void StopMove()
    {
        m_start = false;
    }
    
 
}
