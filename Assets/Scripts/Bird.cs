using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameController m_gamecontrol; 
    [SerializeField] private BirdMove1 m_BirdMove;
    [SerializeField] private AudioSource m_hit;
    [SerializeField] private Rigidbody2D m_rigi; 
    [SerializeField] private AudioSource m_swoosh;
    private Vector2 m_StartPos=new Vector2(-1,-0.31f);
    private const string m_botPile = "BotPipe";
    private const string m_topPile = "TopPipe";
    private const string m_ground = "Ground";
    private Vector3 m_moveLeft = new Vector3(0.5f, 0, 0);
    [SerializeField] private Animator m_animator;
    
    void Start()
    {
        
    }
    public void SetAnimation(int index)
    {
        if(index==1)
        {
            m_animator.SetBool("Check", true);
            Debug.Log("xxxxxxx"); 
        }
    }
    public void StartGame()
    {
        m_BirdMove.StartMove();
        
    }
    public void MoveBack()
    {
        m_BirdMove.Rotateaagian();
        m_animator.enabled = true ;
        transform.position = m_StartPos;
        m_BirdMove.StopMove(); 
    }
    public void StopMove()
    {
        m_BirdMove.StopMove();
        
        
    }
    public void Die()
    {
        m_BirdMove.Die();
        m_swoosh.Play();
        m_animator.enabled = false; 
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.name.Contains(m_botPile)  || collision.gameObject.name.Contains(m_topPile) || collision.gameObject.name.Contains(m_ground) )
        {
            
            m_hit.Play();
            Die();
            m_gamecontrol.GameOver();
            transform.position -=  m_moveLeft;
             
        }
    }
}
 