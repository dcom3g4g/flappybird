using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    // Start is called before the first frame update
    float m_timeToSpawn=2;
    private float m_time = 0f;
    [SerializeField] private bool m_start = false; 
    [SerializeField] GameObject m_topPile;
    [SerializeField] GameObject m_botPile;
    private List<GameObject> m_pileExist= new List<GameObject>();
    private GameObject m_pileCreate;
    private GameObject m_pileCreate1;
    private const int m_minRandom = 4;
    private const int m_maxRandom = 6;
    private const float m_xPos = 4f;
    private const float m_yPos = 8f;
    void Start()
    {
        
    }
    public void StartSpawn()
    {
        m_start = true; 
    }
    public void Stopspawn()
    {
        m_start = false; 
    }
    public void ClearPile()
    {
        ///////
        
        foreach (GameObject targetObject in m_pileExist)
        {
            if(targetObject)
                Destroy(targetObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (m_start)
        {
            m_time += Time.deltaTime;
            if (m_time > m_timeToSpawn)
            {
                m_time = 0f;
                int TopPosition = Random.Range(m_minRandom, m_maxRandom);
                //////////aaaa
                m_pileCreate=  Instantiate(m_topPile, new Vector2(m_xPos, (float)TopPosition), Quaternion.identity);
                m_pileCreate1 = Instantiate(m_botPile, new Vector2(m_xPos, (float)TopPosition - m_yPos), Quaternion.identity);
                m_pileExist.Add(m_pileCreate);
                m_pileExist.Add(m_pileCreate1);
            }
        }
    }
}
