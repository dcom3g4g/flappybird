using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMove : MonoBehaviour
{

    
    private static bool m_start=true;

    private Vector2 m_veloMove = new Vector2(-3, 0);
    private Vector2 m_veloStop = new Vector2(0, 0);
    public bool start { get => m_start; set => m_start = value; }
    
    private const string m_bird = "Bird"; 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transform.position.x < -5)
            Destroy(gameObject);
        
    }
    private void FixedUpdate()
    {
        if (gameObject.GetComponent<Rigidbody2D>() != null)
        {
            if (m_start)
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = m_veloMove;
            }
            else
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = m_veloStop;
            }
            
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name.Contains(m_bird))
        {
            m_start = false;
            
        }
    }

}
