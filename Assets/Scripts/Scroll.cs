using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Scroll : MonoBehaviour
{
    [SerializeField] private bool m_start = false;
    [SerializeField] private PipeMove m_pileMove;
    [SerializeField] private Rigidbody2D m_rigid ;
    [SerializeField] private  float m_posReset = 1.45f ;
    private Vector2 m_veloMove = new Vector2(-3, 0);
    private Vector2 m_veloStop = new Vector2(0, 0);
    // Start is called before the first frame update    
    void Start()
    {

    }
    public void StartScroll()
    {
        m_start = true; 
    }
    public void StopScroll()
    {
        m_start = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        if (m_start)
        {
            m_rigid.velocity = m_veloMove;
        }
        else
        {
            m_rigid.velocity = m_veloStop;
        }
            
        if (transform.position.x < -m_posReset)
        {
            transform.position -= new Vector3(transform.position.x, 0, 0);

        }
    }
}
